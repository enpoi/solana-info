If you don’t have experience with NFTs and crypto, don’t worry, I can help guide you and answer questions. The good news is that you can think of NFTs as video game characters with a picture and some attributes. You’ll access data about them using web APIs.

People buy and sell the characters on marketplaces and view their attributes on rarity trackers.

By examining how these websites are constructed, we can get the data we need.

## Collection Viewers ##

HowRare shows the rarity for different NFT collections on Solana. They also show Sales History. Here is a particular character with Sales History: https://howrare.is/peskypenguinclub/8758/ HowRare has a Discord chat where you could talk to the developer about how they fetch the data: https://discord.gg/VmhsCdjyEx

A similar website is called, MoonRank. Here is the same character on that website: https://moonrank.app/collection/peskypenguins/Ew4LjP2MEbu5qvWbM1Tmx9Eg8fEAL9RZduVZFjxBNVK6 

Those websites are good see all the characters in collections and view their pictures and attributes. 

Another interesting website that dives more into the data and history for each collection is: https://www.hellomoon.io It requires a sign up, but after you’re in, you can look at the same collection there: https://www.hellomoon.io/nfts/pesky-penguins 

## Marketplaces ##

You saw some Sales History from a marketplace on HowRare.

The most important marketplace to work with now is called Magic Eden: https://magiceden.io/ They have an API available here: https://api.magiceden.dev/ 

## API use cases ##

Here are a couple of cool use cases for the API helping people interpret the prices:

* Deal Finder: https://thethirdweb.io/degods
* Scanner: https://dgscan.web.app/  
